package main

import (
	"github.com/gorilla/mux"
	"gitlab.com/garbotron/goshots"
	"log"
	"net/http"
	_ "net/http/pprof"
	"os"
)

func main() {
	r := mux.NewRouter()
	log.Println("Beginning initialization")
	if err := goshots.Init(r); err != nil {
		log.Fatal(err)
		return
	}
	http.Handle("/", r)
	log.Println("Initialization done, starting server")

	port := os.Getenv("GOSHOTS_PORT")
	if port == "" {
		port = "80"
	}
	if err := http.ListenAndServe(":"+port, nil); err != nil {
		log.Fatal(err)
		return
	}
	log.Println("Exiting successfully")
}
