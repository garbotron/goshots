package goshots

import (
	"github.com/gorilla/mux"
	"gitlab.com/garbotron/goshots/core"
	"gitlab.com/garbotron/goshots/providers/animeshots"
	"gitlab.com/garbotron/goshots/providers/gamershots"
	"math/rand"
	"time"
)

func Init(r *mux.Router) error {
	rand.Seed(time.Now().UTC().UnixNano())

	providers := []goshots.Provider{
		&gamershots.Gamershots{},
		&animeshots.Animeshots{},
	}

	return goshots.ServerInit(r, providers...)
}
