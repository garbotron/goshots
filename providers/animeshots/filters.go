package animeshots

import (
	"fmt"
	"gitlab.com/garbotron/goshots/core"
	"gopkg.in/mgo.v2/bson"
	"time"
)

type FilterListType int

const Whitelist FilterListType = 0
const Blacklist FilterListType = 1

func (lt *FilterListType) name() string {
	if *lt == Whitelist {
		return "Whitelist"
	} else {
		return "Blacklist"
	}
}

type AnimeshotsFilter interface {
	goshots.Filter
	Apply(as *Animeshots, vals []int) []bson.M
}

func AnimeshotsFilters() []AnimeshotsFilter {
	return []AnimeshotsFilter{
		asFilterType{},
		asFilterDate{},
		asFilterMainGenre{Whitelist},
		asFilterMainGenre{Blacklist},
		asFilterSubGenre{Whitelist},
		asFilterSubGenre{Blacklist},
		asFilterTag{Whitelist},
		asFilterTag{Blacklist},
		asFilterRating{},
		asFilterNumEpisodes{},
		asFilterRuntime{}}
}

func (_ Animeshots) Filters() []goshots.Filter {
	filters := AnimeshotsFilters()
	ret := make([]goshots.Filter, len(filters))
	for i := 0; i < len(filters); i++ {
		ret[i] = filters[i]
	}
	return ret
}

//----------------------------------------------------------------------------//

type asFilterType struct{}

func (_ asFilterType) Name() string {
	return "Filter by Type"
}

func (_ asFilterType) Prompt() string {
	return ""
}

func (_ asFilterType) Type() goshots.FilterType {
	return goshots.FilterTypeSelectMany
}

func (_ asFilterType) Names(p goshots.Provider) ([]string, error) {
	as := p.(*Animeshots)
	return as.GetAllTypes(), nil
}

func (_ asFilterType) DefaultValues() []int {
	return []int{}
}

func (_ asFilterType) Apply(as *Animeshots, vals []int) []bson.M {
	types := as.GetAllTypes()
	sel := []string{}
	for _, i := range vals {
		if i < len(types) {
			sel = append(sel, types[i])
		}
	}
	return []bson.M{
		bson.M{"type": bson.M{"$in": sel}},
	}
}

//----------------------------------------------------------------------------//

type asFilterDate struct{}

func (_ asFilterDate) Name() string {
	return "Filter by Date"
}

func (_ asFilterDate) Prompt() string {
	return "Original release date"
}

func (_ asFilterDate) Type() goshots.FilterType {
	return goshots.FilterTypeNumberRange
}

func (_ asFilterDate) Names(_ goshots.Provider) ([]string, error) {
	return nil, nil
}

func (_ asFilterDate) DefaultValues() []int {
	return []int{1900, time.Now().Year()}
}

func (_ asFilterDate) Apply(as *Animeshots, vals []int) []bson.M {
	if len(vals) < 2 {
		return []bson.M{}
	}
	return []bson.M{
		bson.M{"hasyear": true},
		bson.M{"year": bson.M{"$gte": vals[0]}},
		bson.M{"year": bson.M{"$lte": vals[1]}},
	}
}

//----------------------------------------------------------------------------//

type asFilterMainGenre struct {
	listType FilterListType
}

func (f asFilterMainGenre) Name() string {
	return fmt.Sprintf("Filter by main genre: %s", f.listType.name())
}

func (_ asFilterMainGenre) Prompt() string {
	return ""
}

func (_ asFilterMainGenre) Type() goshots.FilterType {
	return goshots.FilterTypeSelectMany
}

func (_ asFilterMainGenre) Names(p goshots.Provider) ([]string, error) {
	as := p.(*Animeshots)
	return as.GetAllMainGenres(), nil
}

func (_ asFilterMainGenre) DefaultValues() []int {
	return []int{}
}

func (f asFilterMainGenre) Apply(as *Animeshots, vals []int) []bson.M {
	genres := as.GetAllMainGenres()
	sel := []string{}
	for _, i := range vals {
		if i < len(genres) {
			sel = append(sel, genres[i])
		}
	}
	if f.listType == Blacklist {
		return []bson.M{
			bson.M{"maingenres.0": bson.M{"$exists": true}},
			bson.M{"maingenres": bson.M{"$not": bson.M{"$in": sel}}},
		}
	} else {
		return []bson.M{
			bson.M{"maingenres": bson.M{"$in": sel}},
		}
	}
}

//----------------------------------------------------------------------------//

type asFilterSubGenre struct {
	listType FilterListType
}

func (f asFilterSubGenre) Name() string {
	return fmt.Sprintf("Filter by sub-genre: %s", f.listType.name())
}

func (_ asFilterSubGenre) Prompt() string {
	return ""
}

func (_ asFilterSubGenre) Type() goshots.FilterType {
	return goshots.FilterTypeSelectMany
}

func (_ asFilterSubGenre) Names(p goshots.Provider) ([]string, error) {
	as := p.(*Animeshots)
	return as.GetAllSubGenres(), nil
}

func (_ asFilterSubGenre) DefaultValues() []int {
	return []int{}
}

func (f asFilterSubGenre) Apply(as *Animeshots, vals []int) []bson.M {
	genres := as.GetAllSubGenres()
	sel := []string{}
	for _, i := range vals {
		if i < len(genres) {
			sel = append(sel, genres[i])
		}
	}
	if f.listType == Blacklist {
		return []bson.M{
			bson.M{"subgenres.0": bson.M{"$exists": true}},
			bson.M{"subgenres": bson.M{"$not": bson.M{"$in": sel}}},
		}
	} else {
		return []bson.M{
			bson.M{"subgenres": bson.M{"$in": sel}},
		}
	}
}

//----------------------------------------------------------------------------//

type asFilterTag struct {
	listType FilterListType
}

func (f asFilterTag) Name() string {
	return fmt.Sprintf("Filter by Tag: %s", f.listType.name())
}

func (_ asFilterTag) Prompt() string {
	return ""
}

func (_ asFilterTag) Type() goshots.FilterType {
	return goshots.FilterTypeSelectMany
}

func (_ asFilterTag) Names(p goshots.Provider) ([]string, error) {
	as := p.(*Animeshots)
	return as.GetAllTags(), nil
}

func (_ asFilterTag) DefaultValues() []int {
	return []int{}
}

func (f asFilterTag) Apply(as *Animeshots, vals []int) []bson.M {
	tags := as.GetAllTags()
	sel := []string{}
	for _, i := range vals {
		if i < len(tags) {
			sel = append(sel, tags[i])
		}
	}
	if f.listType == Blacklist {
		return []bson.M{
			bson.M{"tags.0": bson.M{"$exists": true}},
			bson.M{"tags": bson.M{"$not": bson.M{"$in": sel}}},
		}
	} else {
		return []bson.M{
			bson.M{"tags": bson.M{"$in": sel}},
		}
	}
}

//----------------------------------------------------------------------------//

type asFilterRating struct{}

func (_ asFilterRating) Name() string {
	return "Filter by Average Rating (Out of 100)"
}

func (_ asFilterRating) Prompt() string {
	return "Average rating"
}

func (_ asFilterRating) Type() goshots.FilterType {
	return goshots.FilterTypeNumberRange
}

func (_ asFilterRating) Names(_ goshots.Provider) ([]string, error) {
	return nil, nil
}

func (_ asFilterRating) DefaultValues() []int {
	return []int{0, 100}
}

func (_ asFilterRating) Apply(cxt *Animeshots, vals []int) []bson.M {
	if len(vals) < 1 {
		return []bson.M{}
	}
	return []bson.M{
		bson.M{"hasrating": true},
		bson.M{"rating": bson.M{"$gte": vals[0]}},
		bson.M{"rating": bson.M{"$lte": vals[1]}},
	}
}

//----------------------------------------------------------------------------//

type asFilterNumEpisodes struct{}

func (_ asFilterNumEpisodes) Name() string {
	return "Filter by number of episodes"
}

func (_ asFilterNumEpisodes) Prompt() string {
	return "Number of episodes"
}

func (_ asFilterNumEpisodes) Type() goshots.FilterType {
	return goshots.FilterTypeNumberRange
}

func (_ asFilterNumEpisodes) Names(_ goshots.Provider) ([]string, error) {
	return nil, nil
}

func (_ asFilterNumEpisodes) DefaultValues() []int {
	return []int{1, 52}
}

func (_ asFilterNumEpisodes) Apply(cxt *Animeshots, vals []int) []bson.M {
	if len(vals) < 1 {
		return []bson.M{}
	}
	return []bson.M{
		bson.M{"hasnumepisodes": true},
		bson.M{"numepisodes": bson.M{"$gte": vals[0]}},
		bson.M{"numepisodes": bson.M{"$lte": vals[1]}},
	}
}

//----------------------------------------------------------------------------//

type asFilterRuntime struct{}

func (_ asFilterRuntime) Name() string {
	return "Filter by average runtime"
}

func (_ asFilterRuntime) Prompt() string {
	return "Average runtime (minutes)"
}

func (_ asFilterRuntime) Type() goshots.FilterType {
	return goshots.FilterTypeNumberRange
}

func (_ asFilterRuntime) Names(_ goshots.Provider) ([]string, error) {
	return nil, nil
}

func (_ asFilterRuntime) DefaultValues() []int {
	return []int{5, 300}
}

func (_ asFilterRuntime) Apply(cxt *Animeshots, vals []int) []bson.M {
	if len(vals) < 1 {
		return []bson.M{}
	}
	return []bson.M{
		bson.M{"hasruntimeminutes": true},
		bson.M{"runtimeminutes": bson.M{"$gte": vals[0]}},
		bson.M{"runtimeminutes": bson.M{"$lte": vals[1]}},
	}
}
