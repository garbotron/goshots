package animeshots

import (
	"errors"
	"fmt"
	"github.com/PuerkitoBio/goquery"
	"gitlab.com/garbotron/goshots/core"
	"gitlab.com/garbotron/goshots/utils"
	"gopkg.in/mgo.v2"
	"strconv"
	"strings"
)

const numShowScrapers = 30
const maxScreenshotsPerShow = 50

func (as *Animeshots) CanScrape() bool {
	return true
}

func (as *Animeshots) StartScraping(context goshots.ScraperContext) goshots.Scraper {
	db := as.db.DB(MongoDbName)
	shows := db.C(MongoTempShowsCollectionName)
	shows.DropCollection() // in case there was any stale data left over

	s := scraper{
		cxt:         context,
		as:          as,
		shows:       shows,
		listings:    []*ShowListing{},
		proxyTarget: utils.CreateProxyTarget(20),
		stage:       "beginning scan",
		abortSignal: make(chan struct{}, 1),
	}

	go s.scrape()
	return &s
}

func (s *scraper) Abort() {
	if s.aborting {
		s.cxt.Log("already aborting...")
	} else {
		s.aborting = true
		s.abortSignal <- struct{}{}
		s.stage = "aborting"
		s.cxt.Log("aborting...")
	}
}

func (s *scraper) Progress() (stage string, cur int, total int) {
	stage = s.stage
	cur, _ = s.shows.Count()
	total = len(s.listings)
	return
}

// Simple type encapsulating just the data that can be scraped from the listing page
type ShowListing struct {
	ShowId         string
	Name           string
	Type           string
	Year           int
	HasYear        bool
	Rating         int
	HasRating      bool
	NumEpisodes    int
	HasNumEpisodes bool
}

type scraper struct {
	cxt         goshots.ScraperContext
	as          *Animeshots
	listings    []*ShowListing
	shows       *mgo.Collection // full show data used for final result
	proxyTarget *utils.ProxyTarget
	stage       string
	aborting    bool
	abortSignal chan struct{}
}

func (s *scraper) scrape() {
	// we scrape in 2 phases:
	// 1 - look at the listing pages only - collect show name/year/type/rating
	// 2 - for each show, scrape all the other info (screenshots, genres, etc)

	s.cxt.Log("collecting show listings...")
	s.stage = "collecting show list"
	if err := s.scrapeShowListings(); err != nil {
		s.finish(err)
		return
	}

	s.cxt.Log("starting detailed scan...")
	s.stage = "scanning"
	if err := s.scrapeShows(); err != nil {
		s.finish(err)
		return
	}

	s.cxt.Log("committing changes...")
	s.stage = "committing changes"
	if err := s.commitChanges(); err != nil {
		s.finish(err)
		return
	}

	s.cxt.Log("scrape complete! shutting down...")
	s.finish(nil)
}

func (s *scraper) finish(err error) {
	s.proxyTarget.Dispose()
	s.cxt.Done(err)
}

func (s *scraper) scrapeShowListings() error {
	idx := 1
	for {
		s.cxt.Log(fmt.Sprintf("starting collection of listing page %d...", idx))
		num, err := s.scrapeShowListingPage(idx)
		if err != nil {
			s.cxt.Error(fmt.Sprintf("listing-page-%d", idx), err)
			return err
		}
		if num == 0 {
			s.cxt.Log("listing collection complete")
			return nil // we got to the end of the list - we're done
		}
		idx++
	}
}

// Scrapes a single show listing page.  Returns how many shows were scraped, plus any error encountered.
func (s *scraper) scrapeShowListingPage(offset int) (int, error) {
	numShows := 0
	url := fmt.Sprintf("http://www.anisearch.com/anime/index/?limit=100&page=%d", offset)
	doc := s.downloadPage(url)
	var err error = nil
	doc.Find("table.mtC tr").Each(func(_ int, row *goquery.Selection) {
		titleLink := row.Find("th a")
		dataCell := row.Find("td[data-title='Type / Episodes / Year']")
		ratingCell := row.Find("td[data-title='Votes']")
		if titleLink.Length() == 0 || dataCell.Length() == 0 || ratingCell.Length() == 0 {
			err = errors.New("Listing table rows not found")
			return
		}

		href, ok := titleLink.Attr("href")
		if !ok || !strings.HasPrefix(href, "anime/") {
			html, _ := titleLink.Html()
			err = errors.New(fmt.Sprintf("Title link was incorrectly formatted: '%s'", html))
			return
		}

		// the data cell is formatted "<type>, <num-episodes> (<year>)" where any <> can be "?"
		strs := strings.Split(dataCell.Text(), ", ")
		if len(strs) != 2 {
			err = errors.New(fmt.Sprintf("Data cell was incorrectly formatted: '%s'", dataCell.Text()))
			return
		}

		showType := strs[0]
		strs = strings.Split(strs[1], " (")
		if len(strs) != 2 {
			err = errors.New(fmt.Sprintf("Data cell was incorrectly formatted: '%s'", dataCell.Text()))
			return
		}

		numEpisodes := 0
		hasNumEpisodes := false
		numEpisodesStr := strings.TrimSpace(strs[0])
		if numEpisodesStr != "?" {
			hasNumEpisodes = true
			if numEpisodes, err = strconv.Atoi(numEpisodesStr); err != nil {
				return
			}
		}

		year := 0
		hasYear := false
		yearStr := strings.TrimRight(strings.TrimSpace(strs[1]), ")")
		if yearStr != "?" {
			hasYear = true
			if year, err = strconv.Atoi(yearStr); err != nil {
				return
			}
		}

		rating := 0
		hasRating := false
		ratingStr := strings.TrimSpace(ratingCell.Text())
		if ratingStr != "" {
			hasRating = true
			// the rating string is formatted <calculated rating float 0..5> (<num votes>)
			ratingFloat := 0.0
			_, err = fmt.Sscanf(ratingStr, "%f", &ratingFloat)
			if err != nil {
				return
			}
			rating = int(ratingFloat * 20)
		}

		// the show ID is part of the URL: "anime/<show ID>,<show name>"
		showId := href[len("anime/"):len(href)]
		commaIdx := strings.Index(showId, ",")
		showId = showId[0:commaIdx]

		show := &ShowListing{
			ShowId:         showId,
			Name:           titleLink.Text(),
			Type:           showType,
			Year:           year,
			HasYear:        hasYear,
			Rating:         rating,
			HasRating:      hasRating,
			NumEpisodes:    numEpisodes,
			HasNumEpisodes: hasNumEpisodes,
		}

		s.listings = append(s.listings, show)
		numShows++
	})

	if err != nil && s.aborting {
		err = goshots.ScraperAbortError()
	}

	return numShows, err
}

func (s *scraper) scrapeShows() error {
	listings := make(chan *ShowListing)
	done := make(chan struct{})
	for i := 0; i < numShowScrapers; i++ {
		go s.scrapeShowsFromChannel(listings, done)
	}

	for _, show := range s.listings {
		if s.aborting {
			break
		}
		select {
		case listings <- show:
		case <-s.abortSignal:
		}
	}
	close(listings)
	for i := 0; i < numShowScrapers; i++ {
		<-done
	}

	if s.aborting {
		return goshots.ScraperAbortError()
	} else {
		return nil
	}
}

func (s *scraper) scrapeShowsFromChannel(listings <-chan *ShowListing, done chan<- struct{}) {
	for show := range listings {
		if !s.scrapeShow(show) {
			break // aborted
		}
	}
	done <- struct{}{}
}

func (s *scraper) scrapeShow(listing *ShowListing) bool {
	if s.aborting {
		return false
	}

	s.cxt.Log("collecting details for '%s'...", listing.Name)
	show := &Show{
		ShowId:         listing.ShowId,
		Name:           listing.Name,
		Type:           listing.Type,
		Year:           listing.Year,
		HasYear:        listing.HasYear,
		Rating:         listing.Rating,
		HasRating:      listing.HasRating,
		NumEpisodes:    listing.NumEpisodes,
		HasNumEpisodes: listing.HasNumEpisodes}

	url := show.Url()
	doc := s.downloadPage(url)

	// Collect the average runtime
	timeTag := doc.Find("h1 time")
	if timeTag.Length() > 0 {
		timeStr := timeTag.Text()
		if strings.HasSuffix(timeStr, "min") {
			timeStr = timeStr[0 : len(timeStr)-3]
		}
		timeMinutes, err := strconv.Atoi(timeStr)
		if err == nil {
			show.RuntimeMinutes = timeMinutes
			show.HasRuntimeMinutes = true
		} else {
			s.cxt.Error(show.Name, err)
		}
	}

	// Collect the main genres
	doc.Find("li > a.gg").Each(func(_ int, a *goquery.Selection) {
		show.MainGenres = append(show.MainGenres, a.Text())
	})

	// Collect the sub-genres
	doc.Find("li > a.gc").Each(func(_ int, a *goquery.Selection) {
		show.SubGenres = append(show.SubGenres, a.Text())
	})

	// Collect the tags
	doc.Find("li > a.gt").Each(func(_ int, a *goquery.Selection) {
		show.Tags = append(show.Tags, a.Text())
	})

	// Collect the screenshots
	doc.Find("#screens > a").Each(func(_ int, a *goquery.Selection) {
		if len(show.ScreenshotUrls) == maxScreenshotsPerShow {
			return
		}
		dataId, ok := a.Attr("data-id")
		if !ok {
			s.cxt.Error(show.Name, errors.New("data-id not found"))
			return
		}
		category := "0"
		if len(listing.ShowId) > 3 {
			category = listing.ShowId[0 : len(listing.ShowId)-3]
		}
		imageUrl := fmt.Sprintf(
			"http://cdn.anisearch.com/images/anime/screen/%s/%s/full/%s.jpg",
			category,
			listing.ShowId,
			dataId)
		show.ScreenshotUrls = append(show.ScreenshotUrls, imageUrl)
	})

	if len(show.ScreenshotUrls) == 0 {
		s.cxt.Log("skipped '%s' (no screenshots)", show.Name)
		return true
	}

	s.shows.Insert(&show)
	s.cxt.Log("completed '%s' (%d screenshots)", show.Name, len(show.ScreenshotUrls))

	return true
}

func (s *scraper) commitChanges() error {
	db := s.as.db.DB(MongoDbName)
	shows := db.C(MongoShowsCollectionName)
	shows.DropCollection()

	iter := s.shows.Find(nil).Iter()
	result := Show{}
	for iter.Next(&result) {
		if len(result.ScreenshotUrls) > 0 {
			err := shows.Insert(&result)
			if err != nil {
				return err
			}
		}
	}

	db.C(MongoTempShowsCollectionName).DropCollection()
	return nil
}

func (s *scraper) downloadPage(page string) *goquery.Document {
	html := s.proxyTarget.Get(page, func(s string) error {
		if !strings.Contains(s, `<a href="http://www.anisearch.com" id="logo"> </a>`) {
			return errors.New("couldn't find logo")
		}
		return nil
	})
	doc, err := goquery.NewDocumentFromReader(strings.NewReader(html))
	if err != nil {
		s.cxt.Error(page, err)
		doc, _ := goquery.NewDocumentFromReader(strings.NewReader(""))
		return doc
	}
	return doc
}
